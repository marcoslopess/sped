import React from 'react'
import './App.css'
import blocoHJson from './blocoH.json'


let blocoH = ''
let blocoH010 = ''
let blocoH005A = ''
let blocoH005B = ''

let valoMonetario = 0
let valorTotal = 0

let valorContabil = 391720.00
let valorSistema = 74320.51

let multiploQuantidade = (valorContabil / valorSistema).toFixed(9)

//faz um loop dentro do json
blocoHJson.map(function (value) {

  //separa em blocos o json
  let bloco = value.bloco.split("|")

  //separa o bloco H010 DO BLOCO H005 
  if (bloco[0] === "H010") {

    //remove a virgula e transforma o preço de string pra float
    bloco[4] = bloco[4].replace(",", ".")
    let preco = parseFloat(bloco[4])

    //transforma a quantidade de string pra float
    let quantidade = parseFloat(bloco[3])

    let quantidadeAumentada = (quantidade * multiploQuantidade)

    //multiplica a nova quantidade pelo preço
    let valor = (quantidadeAumentada * preco).toFixed(2)

    //calcula o valor total
    if (valor !== isNaN) {
      valorTotal = (parseFloat(valorTotal) + parseFloat(valor)).toFixed(2)
      valoMonetario = valor.replace(".", ",")
      bloco[4] = bloco[4].replace(".", ",")
    }
    else {
      console.error("erro" + value.bloco + "erro");

    }

    //recria o bloco na no padrão do sped
    let blocoEditado = "|" + bloco[0] + "|" + bloco[1] + "|" + bloco[2] + "|" +
      quantidadeAumentada.toFixed() + "|" + bloco[4] + "|" + valoMonetario
      + "|" + bloco[6] + "|||" + bloco[9] + "|" + bloco[10] + "|";

    //coloca o bloco editado em uma variavel global para extração da informação
    blocoH010 += '\n' + blocoEditado

  } else {

    blocoH005A = "\n|" + bloco[0] + "|" + bloco[1] + "|"
    blocoH005B = "|" + bloco[3] + "|"

  }
  return blocoH010
})

valorTotal = valorTotal.replace(".", ",")

//concatena os dois blocos em uma variavel
blocoH = blocoH005A + valorTotal + blocoH005B
  + blocoH010

  console.log(valorTotal);
  
//imprime no console o bloco H no padrão do sped
console.log(blocoH);

export default class App extends React.Component {
  render() {
    return (
      <div className="App">
        <label>
          aperte F12 para ver os resultados
        </label>
      </div>
    );
  }
}
